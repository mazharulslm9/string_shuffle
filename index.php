<?php

function random_string_generator($number) {
    $output = "";
    $letters = "ABCDEFGYIJKLMNOPQRSTWXYZabcdefghijklmnopqrstwxyz";
    $letters_array = str_split($letters);
    for ($i = 0; $i < $number; $i++) {
        $random_letters = array_rand($letters_array);
        $output .= $letters_array[$random_letters];
    }
    return $output;
}
?>
<!DOCTYPE html>
<!--
To change this license header, choose License Headers in Project Properties.
To change this template file, choose Tools | Templates
and open the template in the editor.
-->
<html>
    <head>
        <meta charset="UTF-8">
        <title>String Shuffle</title>
        <link rel="stylesheet" href="./assets/css/bootstrap.min.css">
    </head>
    <body>
        <section>
            <div class="container">
                <div class="row">
                    <div class="col-md-offset-4 col-md-4">
                        <header><h3 class="text-center">Random String Generator</h3></header>
                        <hr>
                        <form class="form-inline text-center" action="" method="post">
                            <div class="form-group">
                                <label for="exampleInputName2">Enter Length::</label>
                                <input id="num" value="<?php if (isset($_POST['number'])) echo $_POST['number'] ?>" type="text" name="number" class="form-control" id="exampleInputName2" placeholder="Enter Length">
                            </div>
                            <div class="form-group">
                                <input class="form-control" type="submit" value="Click" name="submit">

                            </div>

                        </form>
                         <h5 class="pull-left">Output::</h5>
                        <div id="message" class="text-center">
                           
                            <h3>
                                <?php
                                if (isset($_POST['submit'])) {
                                    $number = $_POST['number'];
                                    $random_string = random_string_generator($number);
                                    echo $random_string;
                                }
                                ?>
                            </h3>
                        </div>

                    </div>
                </div>

            </div>
        </section>
        <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
        <script type="text/javascript">
            setTimeout(function () {
                $('#message').fadeOut('slow');
                $('#num').val('');

            }, 8000);

        </script>


    </body>
</html>
